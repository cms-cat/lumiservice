import subprocess
import argparse
import json

def get_pods(base_names=[]):

    pod_names = []

    out,err = subprocess.Popen(["oc","get","pods"],stderr=subprocess.PIPE,stdout=subprocess.PIPE,universal_newlines=True).communicate()

    if err:
        print(err)
        return pod_names

    for line in out.split("\n"):          
        for name in base_names:            
            if line.startswith(name) and line.split()[2]=="Running":
                
                out_pod,out_err = subprocess.Popen(["oc","get","pod",line.split()[0],"-o","jsonpath='{.metadata}'"],
                                                   stderr=subprocess.PIPE,stdout=subprocess.PIPE,universal_newlines=True).communicate()
                if out_pod:
                    pod_data = json.loads(out_pod[1:-1])
                    print(pod_data["labels"]["deployment"])
                    if pod_data["labels"]["deployment"]==name:
                        pod_names.append(line.split()[0])
                if out_err:
                    print(out_err)
                    
    return pod_names

if __name__ == "__main__":    
    parser = argparse.ArgumentParser(description='copies files between pods')
    parser.add_argument("files",nargs="+",help="files to copy")
    args = parser.parse_args()  

    podnames = ["lumiservice-dev"]
    
    pods = get_pods(podnames)
    for pod in pods:
        for filename in args.files:  
            cmd = ["oc","cp",filename,f"{pod}:/opt/app-root/{filename}"]
            print(" ".join(cmd))
            subprocess.Popen(cmd).communicate()