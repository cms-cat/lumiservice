# Service for the computation of luminosity

This is a remote brilcalc service which users can forward commands to a remote instance of brilcalc to avoid having to install brilcalc locally.

## Accessing the api

The api is hosted at [https://cmslumiservice.app.cern.ch/](https://cmslumiservice.app.cern.ch/)

The api is documented using swagger at [https://cmslumiservice.app.cern.ch/docs](https://cmslumiservice.app.cern.ch/docs)

The API is projected and requires a token with an audience of "cat-lumiservice" to access. The token can be obtained from the CERN SSO service. The token should be passed in the "Authorization" header as a bearer token.

A package to simplify this for you is ["https://gitlab.cern.ch/cms-tsg/common/tsgauth"](tsgauth) used by the CMS TSG group and can be installed with pip
```
pip3 install tsgauth
```
An example would be 
```python
import tsgauth
import requests
if __name__ == "__main__":
    normtag="/cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_BRIL.json"
    runNumber="370790"

    auth = tsgauth.oidcauth.KerbAuth(client_id="cat-lumiservice",redirect_url="https://cmslumiservice-dev.app.cern.ch")
    r = requests.put("https://cmslumiservice-dev.app.cern.ch/api/v0/lumi", 
                     json={"normtag":normtag, "runNumber":runNumber}, 
                     headers=auth.headers())
    
    print(r.json("response"))
```

## Deployment & Development

This is hosted on the [https://paas.docs.cern.ch/](CERN paas service) in the cms-lumisevice project.

It is deployed by gitlab ci/cd. The changes to the "test" branch triggers a build of the luimservice docker image with the tag "test". A change to the "master" branch triggers a build of the lumiservice docker image with the tag "master". 

The "test" tag is deployed at cmslumiservice-dev.app.cern.ch while the "master" tag is deployed at cmslumiservice.app.cern.ch.

The docker image is setup such that it can be configured to be hot reloadable by adding "--reload" to the `UVICORN_EXTRA_ARGS` env variable which aids development in the dev instance. 

This means that you can copy over updated versions of the server code to the running pod and have the server automatically restart with the updated changes. This vastly improves development speed as otherwise you would need to rebuild the docker image and redeploy the pod to see the changes.

You can use [https://docs.openshift.com/container-platform/4.13/cli_reference/openshift_cli/getting-started-cli.html](oc) to copy over the files to the running pod. You can find the login command in the [https://paas.cern.ch/](paas page) by clicking on your name in the top right corner and selecting "copy login command"
```
oc project cms-lumiservice #to switch to the correct project
oc get pods #to find the name of the running pod
oc cp <file> <pod_name>:<path_to_copy_to>
```

A script to ease these steps is provided in `tools/copyToPods.py`.

```bash
python3 tools/copyToPods.py main.py 
```

would copy over the main.py file to the development pods which would then update with the new settings.

