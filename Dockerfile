FROM --platform=linux/amd64 gitlab-registry.cern.ch/cms-cloud/brilws-docker as brilws
 
COPY requirements.txt requirements.txt
COPY main.py main.py
COPY start.sh start.sh
COPY fix-permissions fix-permissions

RUN python3 -m pip install --upgrade pip \
 && python3 -m pip install wheel \
 && python3 -m pip install  --disable-pip-version-check --no-cache-dir -r requirements.txt
COPY assemble assemble
EXPOSE 8000

#we copy over to a new directory so we can write to it later for hot reloads in dev
#this is inspired by s2i 
USER root
RUN /home/bril/assemble 
WORKDIR /opt/app-root
ENTRYPOINT [ "/bin/sh","-c","/opt/app-root/start.sh" ]
#CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000","--reload"]
USER 1001
#CMD ["/bin/bash"]
