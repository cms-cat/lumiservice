from typing import Union
from enum import Enum
from typing_extensions import Annotated

from fastapi import FastAPI, Depends, HTTPException, status, Request
from pydantic import BaseModel

import subprocess
import tsgauth
import os

app = FastAPI()


def user_claims(request: Request):
    bearer_token = request.headers.get("Authorization")
    if not bearer_token:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not authenticated",
            headers={"WWW-Authenticate": "Bearer"},
        )
    if not bearer_token.startswith("Bearer "):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication scheme: must be Bearer <token>",
            headers={"WWW-Authenticate": "Bearer"},
        )
    token = bearer_token.split("Bearer ")[1]    
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        claims = tsgauth.oidcauth.parse_token(token,client_id="cat-lumiservice")
        return claims
    except Exception as e:
        raise credentials_exception
    

class NormTagName(str, Enum):
    physicsNormtag = "/cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_PHYSICS.json"
    preliminaryNormtag = "/cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_BRIL.json"

class LumiUnits(str, Enum):
    fbi="/fb"
    pbi="/pb"
    nbi="/nb"
    ubi="/ub"
    mbi="/mb"    

class LumiRequest(BaseModel):
    normtag: Annotated[NormTagName, "The normtag to be used"]
    runNumber: Union[str,None] = "315267"
    trigger: Union[None, str] = None
    units: LumiUnits = "/fb"


@app.get("/")
def read_root():
    return {"msg": "lumiservice is up and running, please access /api/v0/info for more information (authentication required)"}

@app.get("/api/v0/info")
def read_info(claims = Depends(user_claims)):
    process = subprocess.Popen(['brilcalc --version'], shell=True, stdout=subprocess.PIPE)
    process.wait()
    outv, errv = process.communicate()
    process = subprocess.Popen(['brilcalc lumi --help'], shell=True, stdout=subprocess.PIPE)
    outh, errh = process.communicate()
    return {"version": outv, "help": outh}

@app.put("/api/v0/lumi")
def run_lumi(body: LumiRequest,claims = Depends(user_claims)):
    command = f'brilcalc lumi --normtag {body.normtag} -u {body.units} ' 
    if body.runNumber != None:
        command += f'-r {body.runNumber} '
    if body.trigger!= None:
        command += f'--hltpath {body.trigger} '
    print (command)    
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process.wait()
    outv, errv = process.communicate()    
    return {"command": command, 'response': outv, 'error': errv}
    
